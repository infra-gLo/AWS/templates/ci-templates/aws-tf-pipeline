# aws-tf-pipeline

This is a template file intended to provision AWS infrastructure using terraform. When merged, terraform will init, plan, and apply infrasture. The apply and destroy stages require manual intervention. Gitlab is used as a backend to host the state file. A terraform specified gitignore file is also included.

Include this file in your CI/CD build.

# Initiate the backend
To initiate the backend, run these commands or include these in the build itself. You will need to insert your project ID, username, and personal access token.

```
terraform init \
-backend-config="address=https://gitlab.com/api/v4/projects/<project_ID>/terraform/state/" \
-backend-config="lock_address=https://gitlab.com/api/v4/projects/<project_ID>/terraform/state/<YOUR_GENERIC_STATE_NAME>/lock" \
-backend-config="unlock_address=https://gitlab.com/api/v4/projects/<project_ID>/terraform/state/<YOUR_GENERIC_STATE_NAME>/lock" \
-backend-config=username="<USERNAME>" \
-backend-config=password="<GITLAB_TOKEN>" \
-backend-config=lock_method=POST \
-backend-config=unlock_method=DELETE \
-backend-config=retry_wait_min=5
```

The following message (or similar) should appear after these commands are run:

```
Successfully configured the backend "http"! Terraform will automatically
use this backend unless the backend configuration changes.

```

# Project setup

In your project repo, create 2 environment variables (masked and protected). Terraform will automatically load any defined variables that are prefixed with TF_VAR_.

TF_VAR_aws_access_key and TF_VAR_aws_secret_key

Commit/Push project and build should initiate.

# Sources

https://docs.gitlab.com/ee/user/infrastructure/

https://blog.ajbothe.com/storing-your-terraform-state-in-gitlab
